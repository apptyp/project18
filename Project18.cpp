﻿
#include <iostream>
#include <string>

using namespace std;


class User
{
private:
	
	int score;
	string name;

public:
	User()
	{
		score = 0;
		name = "Unname";
	}
	void SetScore(int value)
	{
		score = value;
	}
	int GetScore()
	{
		return score;
	}
	void SetName(string nam)
	{
		name = nam;
	}
	string GetName()
	{
		return name;
	}

};

int main()
{
	int size;
	cout << "Введите количество пользователей" << endl;
	cin >> size;

	cout << "======================================" << endl;
	User* user = new User[size];
	for (int i = 0; i < size; i++)
	{
		cout << "Введите имя пользователя\t";
		string str;
		cin >> str;
		user[i].SetName(str);
		cout << "Введите количество очков\t";
		int vol;
		cin >> vol;
		user[i].SetScore(vol);
	}
	cout << "======================================" << endl;

	int sizewhile = size;
	while (sizewhile--)
	{
		bool swapped = false;

		for (int i = 0; i < size; i++)
		{
			if (user[i].GetScore() < user[i + 1].GetScore())
			{
				swap(user[i], user[i + 1]);
				swapped = true;
			}
		}

		if (swapped == false)
			break;
	}

	for (int i = 0; i < size; i++)
	{
		cout << user[i].GetName() << '\t';
		cout << user[i].GetScore() << endl;
	}

	delete[] user;
}

